/*******************************************************************************
 *  hal_pmm.h -
 ******************************************************************************/

#ifndef __PMM
#define __PMM

#define PMM_STATUS_OK     0
#define PMM_STATUS_ERROR  1

//====================================================================
/**
  * Set the VCore to a new level if it is possible and return a
  * error - value.
  *
  * \param      level       PMM level ID
  * \return	int	    1: error / 0: done
  */
uint16_t SetVCore(uint8_t level);

//====================================================================
/**
  * Set the VCore to a higher level, if it is possible.
  * Return a 1 if voltage at highside (Vcc) is to low
  * for the selected Level (level).
  *
  * \param      level       PMM level ID
  * \return	int	    1: error / 0: done
  */
uint16_t SetVCoreUp(uint8_t level);

//====================================================================
/**
  * Set the VCore to a lower level.
  * Return a 1 if voltage at highside (Vcc) is still to low
  * for the selected Level (level).
  *
  * \param      level       PMM level ID
  * \return	int	    1: done with error / 0: done without error
  */
uint16_t SetVCoreDown(uint8_t level);

#endif /* __PMM */
