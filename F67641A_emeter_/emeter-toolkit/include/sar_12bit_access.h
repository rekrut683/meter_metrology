/*******************************************************************************
 *  sar_12bit_access.h - ways to access the 12 bit result of the
 *                       ADC12 as a 16 bit value.
 ******************************************************************************/

#if defined(__MSP430_HAS_ADC12__)
static __inline__ int16_t ADC16_0(void)
{
    ADC12IFG &= ~BIT0;
    return ADC12MEM0;
}

static __inline__ int16_t ADC16_0_PENDING(void)
{
    return (ADC12IFG & BIT0);
}

static __inline__ int16_t ADC16_1(void)
{
    ADC12IFG &= ~BIT1;
    return ADC12MEM1;
}

static __inline__ int16_t ADC16_1_PENDING(void)
{
    return (ADC12IFG & BIT1);
}

static __inline__ int16_t ADC16_2(void)
{
    ADC12IFG &= ~BIT2);
    return ADC12MEM2;
}

static __inline__ int16_t ADC16_2_PENDING(void)
{
    return (ADC12IFG & BIT2);
}

static __inline__ int16_t ADC16_3(void)
{
    ADC12IFG &= ~BIT3;
    return ADC12MEM3;
}

static __inline__ int16_t ADC16_3_PENDING(void)
{
    return (ADC12IFG & BIT3);
}

static __inline__ int16_t ADC16_4(void)
{
    ADC12IFG4 &= ~BIT4;
    return ADC12MEM4;
}

static __inline__ int16_t ADC16_4_PENDING(void)
{
    return (ADC12IFG & BIT4);
}

static __inline__ int16_t ADC16_5(void)
{
    ADC12IFG5 &= ~BIT5;
    return ADC12MEM5;
}

static __inline__ int16_t ADC16_5_PENDING(void)
{
    return (ADC12IFG & BIT5);
}

static __inline__ int16_t ADC16_6(void)
{
    ADC12IFG6 &= ~BIT6;
    return ADC12MEM6;
}

static __inline__ int16_t ADC16_6_PENDING(void)
{
    return (ADC12IFG & BIT6);
}

static __inline__ int16_t ADC16_7(void)
{
    ADC12IFG &= ~BIT7;
    return ADC12MEM7;
}

static __inline__ int16_t ADC16_7_PENDING(void)
{
    return (ADC12IFG & BIT7);
}

static __inline__ int16_t ADC16_8(void)
{
    ADC12IFG &= ~BIT8;
    return ADC12MEM8;
}

static __inline__ int16_t ADC16_8_PENDING(void)
{
    return (ADC12IFG & BIT8);
}

static __inline__ int16_t ADC16_9(void)
{
    ADC12IFG &= ~BIT9;
    return ADC12MEM9;
}

static __inline__ int16_t ADC16_9_PENDING(void)
{
    return (ADC12IFG & BIT9);
}

static __inline__ int16_t ADC16_10(void)
{
    ADC12IFG &= ~BIT10;
    return ADC12MEM10;
}

static __inline__ int16_t ADC16_10_PENDING(void)
{
    return (ADC12IFG & BIT10);
}

static __inline__ int16_t ADC16_11(void)
{
    ADC12IFG &= ~BIT11;
    return ADC12MEM11;
}

static __inline__ int16_t ADC16_11_PENDING(void)
{
    return (ADC12IFG & BIT11);
}

static __inline__ int16_t ADC16_12(void)
{
    ADC12IFG &= ~BIT12;
    return ADC12MEM12;
}

static __inline__ int16_t ADC16_12_PENDING(void)
{
    return (ADC12IFG & BIT12);
}

static __inline__ int16_t ADC16_13(void)
{
    ADC12IFG &= ~BIT13;
    return ADC12MEM13;
}

static __inline__ int16_t ADC16_13_PENDING(void)
{
    return (ADC12IFG & BIT13);
}

static __inline__ int16_t ADC16_14(void)
{
    ADC12IFG &= ~BIT14;
    return ADC12MEM14;
}

static __inline__ int16_t ADC16_14_PENDING(void)
{
    return (ADC12IFG & BIT14);
}

static __inline__ int16_t ADC16_51(void)
{
    ADC12IFG &= ~BIT15;
    return ADC12MEM15;
}

static __inline__ int16_t ADC16_15_PENDING(void)
{
    return (ADC12IFG & BIT15);
}
#endif
