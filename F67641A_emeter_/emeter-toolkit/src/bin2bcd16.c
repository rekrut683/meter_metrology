/*******************************************************************************
 *  bin2bcd16.c - 16 bit binary to BCD conversion.
 ******************************************************************************/

#include <inttypes.h>
#include <msp430.h>
#include <stdio.h>
#include "emeter-toolkit.h"

void bin2bcd16(uint8_t bcd[3], uint16_t bin)
{
    int i;
    char buf[6 + 1];
    
    //sprintf (buf, "%06d", bin);
    for (i = 0;  i < 3;  i++) {
        bcd[i] = ((buf[2*i] & 0x0F) << 4) | (buf[2*i + 1] & 0x0F);
    }
}
