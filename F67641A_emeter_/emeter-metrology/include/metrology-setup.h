/*******************************************************************************
 *  metrology-setup.h -
 ******************************************************************************/

#if !defined(_METROLOGY_SETUP_H_)
#define _METROLOGY_SETUP_H_

#if defined(__cplusplus)
extern "C"
{
#endif

#if defined(SAG_POWER_DOWN_SUPPORT)
static void sag_power_down_control(void);
#endif

/* Initialise the ADCs for normal operation */
void metrology_init_analog_front_end_normal_mode(void);

#if defined(LIMP_MODE_SUPPORT)
/* Initialise the ADCs for limp mode operation */
void metrology_init_analog_front_end_limp_mode(void);
#endif

int metrology_align_with_nv_data(void);

#if defined(LIMP_MODE_SUPPORT)
void metrology_switch_to_limp_mode(void);
#endif

#if defined(POWER_DOWN_SUPPORT)
void metrology_switch_to_powerfail_mode(void);
#endif

#if defined(__MSP430_HAS_TLV__)
uint8_t *tlv_find(int tag);
#endif

#if defined(__cplusplus)
}
#endif

#endif
