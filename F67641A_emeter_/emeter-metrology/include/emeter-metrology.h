/*******************************************************************************
 *  emeter-metrology.h -
 ******************************************************************************/

#if !defined(_EMETER_METROLOGY_H_)
#define _EMETER_METROLOGY_H_

#if     defined(__MSP430_HAS_SD16_2__) \
    ||  defined(__MSP430_HAS_SD16_3__) \
    ||  defined(__MSP430_HAS_SD16_A3__) \
    ||  defined(__MSP430_HAS_SD16_A4__) \
    ||  defined(__MSP430_HAS_SD16_A6__) \
    ||  defined(__MSP430_HAS_SD16_A7__) \
    ||  defined(__MSP430_HAS_SD24_2__) \
    ||  defined(__MSP430_HAS_SD24_3__) \
    ||  defined(__MSP430_HAS_SD24_4__) \
    ||  defined(__MSP430_HAS_SD24_A2__) \
    ||  defined(__MSP430_HAS_SD24_A3__) \
    ||  defined(__MSP430_HAS_SD24_B__)
#define __HAS_SD_ADC__
#endif

#if defined(__HAS_SD_ADC__)
#define SAMPLE_RATE                         4096
#define SAMPLES_PER_10_SECONDS              40960
#endif

#include "metrology-template.h"
#include "metrology-calibration-template.h"

#include "metrology-types.h"
#include "metrology-readings.h"
#include "metrology-calibration.h"

int metrology_init(void);

/* Disable the ADCs, and bring them to the lowest power state */
void metrology_disable_analog_front_end(void);

int metrology_init_from_nv_data(void);

void metrology_switch_to_normal_mode(void);

#if defined(LIMP_MODE_SUPPORT)
void metrology_limp_normal_detection(void);
#endif

void calculate_phase_readings(int ph);

#if NUM_PHASES > 1  &&  defined(NEUTRAL_MONITOR_SUPPORT)
void calculate_neutral_readings(void);
#endif

#endif
