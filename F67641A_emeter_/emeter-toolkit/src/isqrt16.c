/*******************************************************************************
 *  isqrt16.c - Square root of a 16 bit number.
 ******************************************************************************/

#include <inttypes.h>
#include "emeter-toolkit.h"

uint16_t isqrt16(uint16_t h)
{
    uint16_t x;
    uint16_t y;
    int i;

#if defined(__GNUC__)  &&  defined(__MSP430__)
    x = 0x8000;
    y = 0;
    i = 16;
    __asm__ (
        "1: \n"
        " setc \n"
        " rlc   %x \n"
        " sub   %x,%y \n"
        " jhs   2f \n"
        " add   %x,%y \n"
        " sub   #2,%x \n"
        "2: \n"
        " inc   %x \n"
        " rla   %h \n"
        " rlc   %y \n"
        " rla   %h \n"
        " rlc   %y \n"
        " dec   %[i] \n"
        " jne   1b \n"
        : [x] "+r"(x), [y] "+r"(y)
        : [h] "r"(h), [i] "r"(i));
#else
    x =
    y = 0;
    for (i = 0;  i < 16;  i++)
    {
        x = (x << 1) | 1;
        if (y < x)
            x -= 2;
        else
            y -= x;
        x++;
        y <<= 1;
        if ((h & 0x8000))
            y |= 1;
        h <<= 1;
        y <<= 1;
        if ((h & 0x8000))
            y |= 1;
        h <<= 1;
    }
#endif
    return  x;
}
