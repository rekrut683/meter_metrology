/*******************************************************************************
 *  div48.c - Divide a 48 bit number by a 16 bit number.
 ******************************************************************************/

#include <inttypes.h>
#include <msp430.h>
#include "emeter-toolkit.h"

int32_t div48(int16_t x[3], int16_t y)
{
    /* Divide a 16 bit integer into a 48 bit one. Expect the answer to be no
       greater than 32 bits, so return the answer as a 32 bit integer.
       A somewhat domain specific divide operation, but pretty useful when
       handling dot products. */
    int32_t x1;
    int32_t z;

    /* Avoid any divide by zero trouble */
    if (y == 0)
        return 0;
    x1 = x[2]%y;
    x1 <<= 16;
    x1 |= (uint16_t) x[1];
    z = x1/y;
    x1 = x1%y;
    x1 <<= 16;
    x1 |= (uint16_t) x[0];
    z = (z << 16) + x1/y;
    return z;
}
