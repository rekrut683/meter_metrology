/*******************************************************************************
 *  metrology-decs.h -
 ******************************************************************************/

/*! \file */

#if !defined(_METROLOGY_DECS_H_)
#define _METROLOGY_DECS_H_

#include "metrology-nv-structs.h"

extern const struct calibration_data_s calibration_defaults;
extern const struct info_mem_s nv_parms;

#if defined(__MSP430__)
/*! The maximum per sample change in the voltage signal, before we declare a voltage spike. */
#define MAX_PER_SAMPLE_VOLTAGE_SLEW     4096

#include "metrology-instantiate-adcs.h"
#endif

#define PHASE_SHIFT_FIR_TABLE_ELEMENTS      256

/*! The table of FIR coefficients to produce a range of phase shifts from -1/2 an ADC sample interval
    to +1/2 an ADC sample interval. When the SD16 is used, the hardware phase shifting capability of
    the ADC is used, instead of this table. */
extern const int16_t phase_shift_fir_coeffs[PHASE_SHIFT_FIR_TABLE_ELEMENTS][2];

#endif
