/*******************************************************************************
 *  sigma_delta_24bit_access.h - ways to access the 24 bit result of the various
 *                               sigma-delta variants as a 32 bit value.
 ******************************************************************************/
#include "msp430.h"
#include "driverlib.h"

/*! Overlay 16 and 32 bit values */
typedef union
{
    /*! A 16 bit way to access a 32 bit value */
    int16_t by16[2];
    /*! A 32 bit way to access a 32 bit value */
    int32_t by32;
} sh_lo_t;

#if defined(__MSP430_HAS_SD24_2__)  ||  defined(__MSP430_HAS_SD24_3__)  ||  defined(__MSP430_HAS_SD24_4__)  ||  defined(__MSP430_HAS_SD24_A2__)  ||  defined(__MSP430_HAS_SD24_A3__)
static __inline__ int32_t ADC32_0(void)
{
    sh_lo_t val;

    SD24CCTL0 &= ~(SD24LSBACC | SD24IFG);
    val.by16[1] = (int16_t) SD24MEM0 >> 8;
    SD24CCTL0 |= SD24LSBACC;
    val.by16[0] = (int16_t) SD24MEM0;
    return val.by32;
}

static __inline__ int16_t ADC32_0_PENDING(void)
{
    return (SD24CCTL0 & SD24IFG);
}

static __inline__ int32_t ADC32_1(void)
{
    sh_lo_t val;

    SD24CCTL1 &= ~(SD24LSBACC | SD24IFG);
    val.by16[1] = (int16_t) SD24MEM1 >> 8;
    SD24CCTL1 |= SD24LSBACC;
    val.by16[0] = (int16_t) SD24MEM1;
    return val.by32;
}

static __inline__ int16_t ADC32_1_PENDING(void)
{
    return (SD24CCTL1 & SD24IFG);
}
#endif

#if defined(__MSP430_HAS_SD24_A3__)
static __inline__ int32_t ADC32_2(void)
{
    sh_lo_t val;

    SD24CCTL2 &= ~(SD24LSBACC | SD24IFG);
    val.by16[1] = (int16_t) SD24MEM2 >> 8;
    SD24CCTL2 |= SD24LSBACC;
    val.by16[0] = (int16_t) SD24MEM2;
    return val.by32;
}

static __inline__ int16_t ADC32_2_PENDING(void)
{
    return (SD24CCTL2 & SD24IFG);
}
#endif

#if defined(__MSP430_HAS_SD24_B2__)  ||  defined(__MSP430_HAS_SD24_B3__)  ||  defined(__MSP430_HAS_SD24_B4__)  ||  defined(__MSP430_HAS_SD24_B6__)  ||  defined(__MSP430_HAS_SD24_B7__)
static __inline__ int32_t ADC32_0(void)
{
    return (int32_t) SD24_B_getResults(SD24_BASE, SD24_B_CONVERTER_0) >> 1;
    //return (int32_t) SD24BMEM0_32 >> 1;
}

static __inline__ int16_t ADC32_0_PENDING(void)
{
    return (SD24BIFG & SD24IFG0);
}

static __inline__ int32_t ADC32_1(void)
{
    return (int32_t) SD24_B_getResults(SD24_BASE, SD24_B_CONVERTER_1) >> 1;
    //return (int32_t) SD24BMEM1_32 >> 1;
}

static __inline__ int16_t ADC32_1_PENDING(void)
{
    return (SD24BIFG & SD24IFG1);
}
#endif

#if defined(__MSP430_HAS_SD24_B3__)  ||  defined(__MSP430_HAS_SD24_B4__)  ||  defined(__MSP430_HAS_SD24_B6__)  ||  defined(__MSP430_HAS_SD24_B7__)
static __inline__ int32_t ADC32_2(void)
{
    return (int32_t) SD24_B_getResults(SD24_BASE, SD24_B_CONVERTER_2) >> 1;
    //return (int32_t) SD24BMEM2_32 >> 1;
}

static __inline__ int16_t ADC32_2_PENDING(void)
{
    return (SD24BIFG & SD24IFG2);
}
#endif

#if defined(__MSP430_HAS_SD24_B4__)  ||  defined(__MSP430_HAS_SD24_B6__)  ||  defined(__MSP430_HAS_SD24_B7__)
static __inline__ int32_t ADC32_3(void)
{
    return (int32_t) SD24BMEM3_32 >> 1;
}

static __inline__ int16_t ADC32_3_PENDING(void)
{
    return (SD24BIFG & SD24IFG3);
}
#endif

#if defined(__MSP430_HAS_SD24_B6__)  ||  defined(__MSP430_HAS_SD24_B7__)
static __inline__ int32_t ADC32_4(void)
{
    return (int32_t) SD24BMEM4_32 >> 1;
}

static __inline__ int16_t ADC32_4_PENDING(void)
{
    return (SD24BIFG & SD24IFG4);
}

static __inline__ int32_t ADC32_5(void)
{
    return (int32_t) SD24BMEM5_32 >> 1;
}

static __inline__ int16_t ADC32_5_PENDING(void)
{
    return (SD24BIFG & SD24IFG5);
}
#endif

#if defined(__MSP430_HAS_SD24_B7__)
static __inline__ int32_t ADC32_6(void)
{
    return (int32_t) SD24BMEM6_32 >> 1;
}

static __inline__ int16_t ADC32_6_PENDING(void)
{
    return (SD24BIFG & SD24IFG6);
}
#endif
