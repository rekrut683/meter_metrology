/*******************************************************************************
 *  sar_10bit_access.h - ways to access the 10 bit result of the
 *                       ADC10 as a 16 bit value.
 ******************************************************************************/

#if defined(__MSP430_HAS_ADC10__)
static __inline__ int16_t ADC16_0(void)
{
    SD16CCTL0 &= ~SD16IFG;
    return ADC10MEM0;
}

static __inline__ int16_t ADC16_0_PENDING(void)
{
    return (SD16CCTL0 & SD16IFG);
}

static __inline__ int16_t ADC16_1(void)
{
    SD16CCTL1 &= ~SD16IFG;
    return ADC10MEM1;
}

static __inline__ int16_t ADC16_1_PENDING(void)
{
    return (SD16CCTL1 & SD16IFG);
}

static __inline__ int16_t ADC16_2(void)
{
    SD16CCTL2 &= ~SD16IFG;
    return ADC10MEM2;
}

static __inline__ int16_t ADC16_3(void)
{
    SD16CCTL3 &= ~SD16IFG;
    return ADC10MEM3;
}

static __inline__ int16_t ADC16_4(void)
{
    SD16CCTL4 &= ~SD16IFG;
    return ADC10MEM4;
}

static __inline__ int16_t ADC16_5(void)
{
    SD16CCTL5 &= ~SD16IFG;
    return ADC10MEM5;
}

static __inline__ int16_t ADC16_6(void)
{
    SD16CCTL6 &= ~SD16IFG;
    return ADC10MEM6;
}

static __inline__ int16_t ADC16_7(void)
{
    SD16CCTL7 &= ~SD16IFG;
    return ADC10MEM7;
}

static __inline__ int16_t ADC16_8(void)
{
    SD16CCTL8 &= ~SD16IFG;
    return ADC10MEM8;
}

static __inline__ int16_t ADC16_9(void)
{
    SD16CCTL9 &= ~SD16IFG;
    return ADC10MEM9;
}

static __inline__ int16_t ADC16_10(void)
{
    SD16CCTL10 &= ~SD16IFG;
    return ADC10MEM10;
}
#endif
