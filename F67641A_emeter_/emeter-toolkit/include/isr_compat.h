/*******************************************************************************
 *  isr_compat.h -
 ******************************************************************************/

#ifndef _ISR_COMPAT_H_
#define _ISR_COMPAT_H_

/* Cross compiler interrupt service routine compatibility definitions */
/* This code currently allows for:
        MSPGCC - the GNU tools for the MSP430
        Quadravox AQ430
        IAR Version 1 (old syntax)
        IAR Versions 2 and 3 (new syntax)
        Rowley Crossworks
        Code Composer Essentials

   These macros allow us to define interrupt routines for all
   compilers with a common syntax:

    ISR(<interrupt>, <routine name>)
    {
    }

   e.g.

    ISR(ADC12, adc_service_routine)
    {
        ADC12CTL0 &= ~ENC;
        ADC12CTL0 |= ENC;
    }
*/
#if defined(__AQCOMPILER__)
    /* This is the Quadravox compiler */
#define ISR(a,b) void _INTERRUPT[a##_VECTOR] b(void)
#elif defined(__IAR_SYSTEMS_ICC__)  &&  (((__TID__ >> 8) & 0x7f) == 43)  &&  (__VER__ < 200)
    /* This is V1.xx of the IAR compiler. */
#define ISR(a,b) interrupt[a##_VECTOR] void b(void)
#elif defined(__IAR_SYSTEMS_ICC__)  &&  (((__TID__ >> 8) & 0x7f) == 43)  &&  (__VER__ < 700)
	/* A tricky #define to stringify _Pragma parameters */
#define __PRAGMA__(x) _Pragma(#x)
    /* This is V2.xx, V3.xx, V4.xx, or V5.xx of the IAR compiler. */
#define ISR(a,b) \
__PRAGMA__(vector=a##_VECTOR) \
__interrupt void b(void)
#elif defined(__CROSSWORKS_MSP430)
    /* This is the Rowley Crossworks compiler */
#define ISR(a,b) void b __interrupt[a##_VECTOR](void)
#elif defined(__TI_COMPILER_VERSION__)
    /* This is the Code Composer Studio compiler. */
	/* A tricky #define to stringify _Pragma parameters */
#define __PRAGMA__(x) _Pragma(#x)
#define ISR(a,b) \
__PRAGMA__(vector=a##_VECTOR) \
__interrupt void a##_ISR(void)
#elif defined(__TI_COMPILER_VERSION)
    /* This is the Code Composer Essentials compiler. */
#define ISR(a,b) __interrupt void b(void); \
a##_ISR(b) \
__interrupt void b(void)
#elif defined(__GNUC__)  &&  defined(__MSP430__)
    /* This is the MSPGCC compiler */
#define ISR(a,b) __attribute__((interrupt(a##_VECTOR))) b(void)
#else
    #error Compiler not recognised.
#endif

#endif
