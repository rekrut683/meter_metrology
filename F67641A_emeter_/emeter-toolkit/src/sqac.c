/*******************************************************************************
 *  sqac.c -
 ******************************************************************************/

#include <inttypes.h>
#include "emeter-toolkit.h"

void sqac48_16(register int16_t z[3], register int16_t x)
{
    int64_t tmp;

    tmp = z[2];
    tmp = tmp << 16;
    tmp |= z[1];
    tmp = tmp << 16;
    tmp |= z[0];
    tmp += (int32_t) x*(int32_t) x;
    z[2] = (tmp >> 32) & 0xFFFF;
    z[1] = (tmp >> 16) & 0xFFFF;
    z[0] = tmp & 0xFFFF;
}

void sqac64_24(int64_t *z, int32_t x)
{
    *z += (int64_t) x*(int64_t) x;
}
