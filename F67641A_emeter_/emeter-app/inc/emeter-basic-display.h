/*******************************************************************************
 *  emeter-basic_display.h -
 ******************************************************************************/

/*! \file */

#if !defined(_EMETER_BASIC_DISPLAY_H_)
#define _EMETER_BASIC_DISPLAY_H_

#if defined(LCD_BARGRAPH_SUPPORT)
void bar_graph(int strength);
#endif

void update_display(void);

#endif
