/*******************************************************************************
 *  metrology-background.h -
 ******************************************************************************/

#if !defined(_METROLOGY_BACKGROUND_H_)
#define _METROLOGY_BACKGROUND_H_

#if defined(TEMPERATURE_SUPPORT)
extern int32_t raw_temperature_from_adc;

extern int16_t temperature_in_celsius;

/*! Get a random number, if available, from the true random number generator, based on Gaussian
    noise in the LSB of the thermal diode. This is only available if the temperature is being measured.
    This return -1 for no number available, or 0 for a fresh random number has been returned as *val. */
int trng(uint16_t *val);

/*! True random number generator, based on Gaussian noise in the LSB of the thermal diode. This
    is only available if the temperature is being measured. This routine will wait for a new random
    number if none is available. */
uint16_t trng_wait(void);
#endif

#if !defined(custom_adc_interrupt)
extern void custom_adc_interrupt(void);
#endif

#endif
