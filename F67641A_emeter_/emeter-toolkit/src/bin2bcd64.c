/*******************************************************************************
 *  bin2bcd64.c - 64 bit binary to BCD conversion.
 ******************************************************************************/

#include <inttypes.h>
#include <msp430.h>
#include <stdio.h>
#include "emeter-toolkit.h"

void bin2bcd64(uint8_t bcd[10], uint64_t bin)
{
    int i;
    char buf[20 + 1];
    
    //sprintf (buf, "%020lld", bin);
    for (i = 0;  i < 10;  i++) {
        bcd[i] = ((buf[2*i] & 0x0F) << 4) | (buf[2*i + 1] & 0x0F);
    }
}   
