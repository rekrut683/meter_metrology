/*******************************************************************************
 *  emeter-lcd.h -
 ******************************************************************************/

/*! \file */

#if !defined(_EMETER_LCD_H_)
#define _EMETER_LCD_H_

typedef uint16_t lcd_starburst_cell_t;
typedef uint8_t lcd_7segmnent_cell_t;

void lcd_init(void);
void lcd_sleep(void);
void lcd_awaken(void);

void lcd_clear(void);

void lcd_cell(uint8_t pattern, int pos);

void lcd_icon(int code, int on);

void lcd_char(char c, int field, int pos);
void lcd_string(const char *s, int field);
void lcd_string_only(const char *s, int field, int pos);

void lcd_decu16_sub_field(uint16_t value, int field, int after, int start, int width);

void lcd_dec16(int16_t value, int field, int after);
void lcd_dec32(int32_t value, int field, int after);
void lcd_dec64(int64_t value, int field, int after);
void lcd_decu16(uint16_t value, int field, int after);
void lcd_decu32(uint32_t value, int field, int after);
void lcd_decu64(uint64_t value, int field, int after);

#endif
