/*******************************************************************************
 *  emeter-metrology-internal.h -
 ******************************************************************************/

#if !defined(_EMETER_METROLOGY_INTERNAL_H_)
#define _EMETER_METROLOGY_INTERNAL_H_

#include <metrology-calibration-template.h>
#include <metrology-template.h>

#if     defined(__MSP430_HAS_SD16_2__) \
    ||  defined(__MSP430_HAS_SD16_3__) \
    ||  defined(__MSP430_HAS_SD16_A3__) \
    ||  defined(__MSP430_HAS_SD16_A4__) \
    ||  defined(__MSP430_HAS_SD16_A6__) \
    ||  defined(__MSP430_HAS_SD16_A7__) \
    ||  defined(__MSP430_HAS_SD24_2__) \
    ||  defined(__MSP430_HAS_SD24_3__) \
    ||  defined(__MSP430_HAS_SD24_4__) \
    ||  defined(__MSP430_HAS_SD24_A2__) \
    ||  defined(__MSP430_HAS_SD24_A3__) \
    ||  defined(__MSP430_HAS_SD24_B__)
#define __HAS_SD_ADC__
#endif

#if defined(__HAS_SD_ADC__)
#define SAMPLE_RATE                         4096
#define ADC_BITS                            16
#define SAMPLES_PER_10_SECONDS              40960
#define LIMP_SAMPLES_PER_10_SECONDS         10240
#define LIMP_SAMPLING_RATIO                 4
#endif

#include "metrology-interactions.h"

#include "metrology-types.h"

#include "metrology-structs.h"

#include "metrology-nv-structs.h"

#include "metrology-foreground.h"

#include "metrology-background.h"

#include "metrology-corrections.h"

#include "metrology-setup.h"

#include "metrology-flash.h"

#endif
