/*******************************************************************************
 *  imul16.c -
 ******************************************************************************/

#include <inttypes.h>
#include "emeter-toolkit.h"

int32_t imul16(register int16_t x, register int16_t y)
{
    int32_t z;

#if defined(__GNUC__)  &&  defined(__MSP430__)
#if (defined(__MSP430_HAS_MPY__)  ||  defined(__MSP430_HAS_MPY32__))  &&  !defined(__TOOLKIT_USE_SOFT_MPY__)
    __asm__ (
"__MPY=0x130 \n"
"__MPYS=0x132 \n"
"__MAC=0x134 \n"
"__MACS=0x136 \n"
"__OP2=0x138 \n"
"__RESLO=0x13a \n"
"__RESHI=0x13c \n"
"__SUMEXT=0x13e \n"
        " push.w  r2 \n"
        " dint \n"
        " mov   %[x],&__MPYS \n"
        " mov   %[y],&__OP2 \n"
        " mov   &__RESHI,%B[z] \n"
        " mov   &__RESLO,%A[z] \n"
        " pop.w r2 \n"
        : [z] "=r"(z)
        : [x] "r"(x), [y] "r"(y));
#else
    register int16_t x1;

    z = 0;
    x1 = 0;
    __asm__ (
        " tst   %[x] \n"
        " jge   2f \n"
        " mov   #-1,%[x1] \n"
        " jmp   2f \n"
        "6: \n"
        " add   %[x],%A[z] \n"
        " addc  %[x1],%B[z] \n"
        "1: \n"
        " rla   %[x] \n"
        " rlc   %[x1] \n"
        "2: \n"
        " rra   %[y] \n"
        " jc    5f \n"
        " jne   1b \n"
        " jmp   4f \n"
        "5: \n"
        " sub   %[x],%A[z] \n"
        " subc  %[x1],%B[z] \n"
        "3: \n"
        " rla   %[x] \n"
        " rlc   %[x1] \n"
        " rra   %[y] \n"
        " jnc   6b \n"
        " cmp   #0xFFFF,%[y] \n"
        " jne   3b \n"
        "4: \n"
        : [z] "+r"(z), [x1] "+r"(x1)
        : [x] "r"(x), [y] "r"(y));
#endif
#else
    z = (int32_t) x*(int32_t) y;
#endif
    return z;
}
