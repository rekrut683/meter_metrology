/*******************************************************************************
 *  isqrt64i.c - Square root of a 64 bit number.
 ******************************************************************************/

#include <inttypes.h>
#include "emeter-toolkit.h"

#if defined(EMETER_TOOLKIT_SUPPORT_64BIT)
uint32_t isqrt64i(uint64_t h)
{
    uint64_t res64;
    uint32_t res32;

    res64 = isqrt64(h);
    res32 = res64 >> 32;
    if (res64 & 0x80000000LLU)
    {
        if (res32 != 0xFFFFFFFFLU)
            return res32 + 1;
    }
    return res32;
}
#endif
