/*******************************************************************************
 *  sigma_delta_16bit_access.h - ways to access the 16 bit result of the various
 *                               sigma-delta variants as a 16 bit value.
 ******************************************************************************/

#if defined(__MSP430_HAS_SD16_2__)  ||  defined(__MSP430_HAS_SD16_3__) \
    || \
    defined(__MSP430_HAS_SD16_A3__)  ||  defined(__MSP430_HAS_SD16_A4__)  ||  defined(__MSP430_HAS_SD16_A6__)  ||  defined(__MSP430_HAS_SD16_A7__)
static __inline__ int16_t ADC16_0(void)
{
    SD16CCTL0 &= ~SD16IFG;
    return SD16MEM0;
}

static __inline__ int16_t ADC16_0_PENDING(void)
{
    return (SD16CCTL0 & SD16IFG);
}

static __inline__ int16_t ADC16_1(void)
{
    SD16CCTL1 &= ~SD16IFG;
    return SD16MEM1;
}

static __inline__ int16_t ADC16_1_PENDING(void)
{
    return (SD16CCTL1 & SD16IFG);
}
#endif

#if defined(__MSP430_HAS_SD16_3__) \
    || \
    defined(__MSP430_HAS_SD16_A3__)  ||  defined(__MSP430_HAS_SD16_A4__)  ||  defined(__MSP430_HAS_SD16_A6__)  ||  defined(__MSP430_HAS_SD16_A7__)
static __inline__ int16_t ADC16_2(void)
{
    SD16CCTL2 &= ~SD16IFG;
    return SD16MEM2;
}

static __inline__ int16_t ADC16_2_PENDING(void)
{
    return (SD16CCTL2 & SD16IFG);
}
#endif

#if defined(__MSP430_HAS_SD16_A4__)  ||  defined(__MSP430_HAS_SD16_A6__)  ||  defined(__MSP430_HAS_SD16_A7__)
static __inline__ int16_t ADC16_3(void)
{
    SD16CCTL3 &= ~SD16IFG;
    return SD16MEM3;
}

static __inline__ int16_t ADC16_3_PENDING(void)
{
    return (SD16CCTL3 & SD16IFG);
}
#endif

#if defined(__MSP430_HAS_SD16_A6__)  ||  defined(__MSP430_HAS_SD16_A7__)
static __inline__ int16_t ADC16_4(void)
{
    SD16CCTL4 &= ~SD16IFG;
    return SD16MEM4;
}

static __inline__ int16_t ADC16_4_PENDING(void)
{
    return (SD16CCTL4 & SD16IFG);
}

static __inline__ int16_t ADC16_5(void)
{
    SD16CCTL5 &= ~SD16IFG;
    return SD16MEM5;
}

static __inline__ int16_t ADC16_5_PENDING(void)
{
    return (SD16CCTL5 & SD16IFG);
}
#endif

#if defined(__MSP430_HAS_SD16_A7__)
static __inline__ int16_t ADC16_6(void)
{
    SD16CCTL6 &= ~SD16IFG;
    return SD16MEM6;
}

static __inline__ int16_t ADC16_6_PENDING(void)
{
    return (SD16CCTL6 & SD16IFG);
}
#endif

#if defined(__MSP430_HAS_SD24_2__)  ||  defined(__MSP430_HAS_SD24_3__)  ||  defined(__MSP430_HAS_SD24_4__)  ||  defined(__MSP430_HAS_SD24_A2__)  ||  defined(__MSP430_HAS_SD24_A3__)
static __inline__ int16_t ADC16_0(void)
{
    SD24CCTL0 &= ~SD24IFG;
    return SD24MEM0;
}

static __inline__ int16_t ADC16_0_PENDING(void)
{
    return (SD24CCTL0 & SD24IFG);
}

static __inline__ int16_t ADC16_1(void)
{
    SD24CCTL1 &= ~SD24IFG;
    return SD24MEM1;
}

static __inline__ int16_t ADC16_1_PENDING(void)
{
    return (SD24CCTL1 & SD24IFG);
}
#endif

#if defined(__MSP430_HAS_SD24_A3__)
static __inline__ int16_t ADC16_2(void)
{
    SD24CCTL2 &= ~SD24IFG;
    return SD24MEM2;
}

static __inline__ int16_t ADC16_2_PENDING(void)
{
    return (SD24CCTL2 & SD24IFG);
}
#endif

#if defined(__MSP430_HAS_SD24_B2__)  ||  defined(__MSP430_HAS_SD24_B3__)  ||  defined(__MSP430_HAS_SD24_B4__)  ||  defined(__MSP430_HAS_SD24_B6__)  ||  defined(__MSP430_HAS_SD24_B7__)
static __inline__ int16_t ADC16_0(void)
{
    SD24BIFG &= ~SD24IFG0;
    return SD24BMEMH0;
}

static __inline__ int16_t ADC16_0_PENDING(void)
{
    return (SD24BIFG & SD24IFG0);
}

static __inline__ int16_t ADC16_1(void)
{
    SD24BIFG &= ~SD24IFG1;
    return SD24BMEMH1;
}

static __inline__ int16_t ADC16_1_PENDING(void)
{
    return (SD24BIFG & SD24IFG1);
}
#endif

#if defined(__MSP430_HAS_SD24_B3__)  ||  defined(__MSP430_HAS_SD24_B4__)  ||  defined(__MSP430_HAS_SD24_B6__)  ||  defined(__MSP430_HAS_SD24_B7__)
static __inline__ int16_t ADC16_2(void)
{
    SD24BIFG &= ~SD24IFG2;
    return SD24BMEMH2;
}

static __inline__ int16_t ADC16_2_PENDING(void)
{
    return (SD24BIFG & SD24IFG2);
}
#endif

#if defined(__MSP430_HAS_SD24_B4__)  ||  defined(__MSP430_HAS_SD24_B6__)  ||  defined(__MSP430_HAS_SD24_B7__)
static __inline__ int16_t ADC16_3(void)
{
    SD24BIFG &= ~SD24IFG3;
    return SD24BMEMH3;
}

static __inline__ int16_t ADC16_3_PENDING(void)
{
    return (SD24BIFG & SD24IFG3);
}
#endif

#if defined(__MSP430_HAS_SD24_B6__)  ||  defined(__MSP430_HAS_SD24_B7__)
static __inline__ int16_t ADC16_4(void)
{
    SD24BIFG &= ~SD24IFG4;
    return SD24BMEMH4;
}

static __inline__ int16_t ADC16_4_PENDING(void)
{
    return (SD24BIFG & SD24IFG4);
}

static __inline__ int16_t ADC16_5(void)
{
    SD24BIFG &= ~SD24IFG5;
    return SD24BMEMH5;
}

static __inline__ int16_t ADC16_5_PENDING(void)
{
    return (SD24BIFG & SD24IFG5);
}
#endif

#if defined(__MSP430_HAS_SD24_B7__)
static __inline__ int16_t ADC16_6(void)
{
    SD24BIFG &= ~SD24IFG6;
    return SD24BMEMH6;
}

static __inline__ int16_t ADC16_6_PENDING(void)
{
    return (SD24BIFG & SD24IFG6);
}
#endif
