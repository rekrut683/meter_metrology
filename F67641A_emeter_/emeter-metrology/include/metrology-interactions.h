/*******************************************************************************
 *  metrology-interactions.h -
 ******************************************************************************/

/* Some features demand others as a prerequisite. Resolve these issues */

#if defined(ACTIVE_ENERGY_PULSES_PER_KW_HOUR)
#define ACTIVE_ENERGY_SUPPORT
#endif

#if defined(ACTIVE_ENERGY_SUPPORT)
#define ACTIVE_POWER_SUPPORT
#endif

#if defined(REACTIVE_ENERGY_PULSES_PER_KVAR_HOUR)
#define REACTIVE_ENERGY_SUPPORT
#endif
#if defined(REACTIVE_ENERGY_SUPPORT)
#define REACTIVE_POWER_SUPPORT
#endif

#if defined(APPARENT_ENERGY_PULSES_PER_KVA_HOUR)
#define APPARENT_ENERGY_SUPPORT
#endif
#if defined(APPARENT_ENERGY_SUPPORT)
#define APPARENT_POWER_SUPPORT
#endif

#if defined(ACTIVE_ENERGY_SUPPORT)  ||  defined(REACTIVE_ENERGY_SUPPORT)  ||  defined(APPARENT_ENERGY_SUPPORT)
#define ENERGY_SUPPORT
#endif

#if defined(ACTIVE_ENERGY_PULSES_PER_KW_HOUR)  ||  defined(REACTIVE_ENERGY_PULSES_PER_KVAR_HOUR)  ||  defined(APPARENT_ENERGY_PULSES_PER_KVA_HOUR)
#define ENERGY_PULSE_SUPPORT
#endif

#if defined(TOTAL_ACTIVE_ENERGY_PULSES_PER_KW_HOUR)
#define TOTAL_ACTIVE_ENERGY_SUPPORT
#endif

#if defined(TOTAL_REACTIVE_ENERGY_PULSES_PER_KVAR_HOUR)
#define TOTAL_REACTIVE_ENERGY_SUPPORT
#endif

#if defined(TOTAL_APPARENT_ENERGY_PULSES_PER_KVA_HOUR)
#define TOTAL_APPARENT_ENERGY_SUPPORT
#endif

#if defined(TOTAL_ACTIVE_ENERGY_PULSES_PER_KW_HOUR)  ||  defined(TOTAL_REACTIVE_ENERGY_PULSES_PER_KVAR_HOUR)  ||  defined(TOTAL_APPARENT_ENERGY_PULSES_PER_KVA_HOUR)
#define TOTAL_ENERGY_PULSE_SUPPORT
#endif

#if defined(TOTAL_ACTIVE_ENERGY_SUPPORT)
#define TOTAL_ACTIVE_POWER_SUPPORT
#endif

#if defined(TOTAL_REACTIVE_ENERGY_SUPPORT)
#define TOTAL_REACTIVE_POWER_SUPPORT
#endif

#if defined(TOTAL_APPARENT_ENERGY_SUPPORT)
#define TOTAL_APPARENT_POWER_SUPPORT
#endif

#if defined(TOTAL_ACTIVE_POWER_SUPPORT)  ||  defined(TOTAL_REACTIVE_POWER_SUPPORT)  ||  defined(TOTAL_APPARENT_POWER_SUPPORT)
#define TOTAL_POWER_SUPPORT
#endif

#if defined(TOTAL_ACTIVE_ENERGY_SUPPORT)  ||  defined(TOTAL_REACTIVE_ENERGY_SUPPORT)  ||  defined(TOTAL_APPARENT_ENERGY_SUPPORT)
#define TOTAL_ENERGY_SUPPORT
#endif

#if defined(TOTAL_POWER_SUPPORT)  ||  defined(TOTAL_ENERGY_SUPPORT)
#define TOTALS_SUPPORT
#endif

#if defined(TOTAL_REACTIVE_POWER_SUPPORT)
#define REACTIVE_POWER_SUPPORT
#endif

#if defined(TOTAL_APPARENT_POWER_SUPPORT)
#define APPARENT_POWER_SUPPORT
#endif

#if defined(VOLTAGE_THD_SUPPORT)
#define FUNDAMENTAL_VRMS_SUPPORT
#endif

#if defined(CURRENT_THD_SUPPORT)
#define FUNDAMENTAL_IRMS_SUPPORT
#endif

#if defined(FUNDAMENTAL_IRMS_SUPPORT)
#define FUNDAMENTAL_ACTIVE_POWER_SUPPORT
#define FUNDAMENTAL_REACTIVE_POWER_SUPPORT
#endif

#if defined(FUNDAMENTAL_ACTIVE_POWER_SUPPORT)  ||  defined(FUNDAMENTAL_REACTIVE_POWER_SUPPORT)
#define FUNDAMENTAL_POWER_SUPPORT
#endif

#if defined(FUNDAMENTAL_ACTIVE_ENERGY_SUPPORT)  ||  defined(FUNDAMENTAL_REACTIVE_ENERGY_SUPPORT)
#define FUNDAMENTAL_ENERGY_SUPPORT
#endif

#if defined(APPARENT_POWER_SUPPORT)
    #if !defined(IRMS_SUPPORT)
#define IRMS_SUPPORT
    #endif
    #if !defined(VRMS_SUPPORT)
#define VRMS_SUPPORT
    #endif
#endif

#if defined(TRNG_SUPPORT)
#define TEMPERATURE_SUPPORT)
#endif

#if NUM_PHASES == 1  &&  defined(TOTAL_POWER_SUPPORT)
#error TOTAL_POWER_SUPPORT selected for single phase metrology
#endif

/* The current channel is always zero unless we are building single phase metrology
   for anti-tamper applications. */
#if NUM_PHASES == 1  &&  defined(NEUTRAL_MONITOR_SUPPORT)
#define PER_PHASE_CURRENT_CHANNELS      2
#else
#define PER_PHASE_CURRENT_CHANNELS      1
#endif

/* The number of scaling factors, DC estimates, etc. per voltage or current channel
   depends on whether limp mode is supported. */
#if defined(LIMP_MODE_SUPPORT)
#define PER_CHANNEL_FACTORS             2
#else
#define PER_CHANNEL_FACTORS             1
#endif
