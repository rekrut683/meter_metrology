/*******************************************************************************
 *  metrology-calibration-template.h - MSP430F67641 3-phase distribution version
 ******************************************************************************/



#define CALADC10_20V_30C                            *((unsigned int *)0x1A1E)
#define CALADC10_20V_85C                            *((unsigned int *)0x1A20)

#define DEFAULT_V_RMS_SCALE_FACTOR_A                32729
#define DEFAULT_V_DC_ESTIMATE_A                     20500
#define DEFAULT_V_AC_OFFSET_A                       9

#define DEFAULT_V_RMS_SCALE_FACTOR_B                32652
#define DEFAULT_V_DC_ESTIMATE_B                     20500
#define DEFAULT_V_AC_OFFSET_B                       9

#define DEFAULT_V_RMS_SCALE_FACTOR_C                32717
#define DEFAULT_V_DC_ESTIMATE_C                     20500
#define DEFAULT_V_AC_OFFSET_C                       9

#define DEFAULT_I_RMS_SCALE_FACTOR_A                23540
#define DEFAULT_I_DC_ESTIMATE_A                     0
#define DEFAULT_I_AC_OFFSET_A                       29000

#define DEFAULT_I_RMS_SCALE_FACTOR_B                23553
#define DEFAULT_I_DC_ESTIMATE_B                     0
#define DEFAULT_I_AC_OFFSET_B                       29000

#define DEFAULT_I_RMS_SCALE_FACTOR_C                23513
#define DEFAULT_I_DC_ESTIMATE_C                     0
#define DEFAULT_I_AC_OFFSET_C                       29000

/* The following will be assigned automatically if they are not set here */
#define DEFAULT_P_SCALE_FACTOR_A                    24766
#define DEFAULT_P_SCALE_FACTOR_B                    25008
#define DEFAULT_P_SCALE_FACTOR_C                    24652

/* Value is phase angle in 1/256th of a sample increments. */
#define DEFAULT_BASE_PHASE_A_CORRECTION             (-207)
#define DEFAULT_BASE_PHASE_B_CORRECTION             (-207)
#define DEFAULT_BASE_PHASE_C_CORRECTION             (-216)

/*The below macros are not used in the code.  The phase and gain macros below are not applicable to the TI F67641 EVM.
  The temperature macros could be used to switch to a more accurate way of calculating temperature(assuming the proper
  values of the slope and intercept macros, which may be device dependent.*/
    /*! This defines the default offset for the temperature diode, in ADC units */
    #define DEFAULT_TEMPERATURE_INTERCEPT               20830

    /*! This defines the default scaling factor for the temperature diode, in ADC units */
    #define DEFAULT_TEMPERATURE_SLOPE                   4400
    #define DEFAULT_ROOM_TEMPERATURE                    250
    #define DEFAULT_PHASE_CORRECTION1                   0
    #define DEFAULT_GAIN_CORRECTION1                    0
    #define DEFAULT_PHASE_CORRECTION2                   0
    #define DEFAULT_GAIN_CORRECTION2                    0

    #define DEFAULT_FREQUENCY_PHASE_FACTOR              500
    #define DEFAULT_FREQUENCY_GAIN_FACTOR               0
