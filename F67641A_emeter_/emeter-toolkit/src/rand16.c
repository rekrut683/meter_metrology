/*******************************************************************************
 *  rand16.c - Generate 16 bit pseudo-random numbers.
 ******************************************************************************/

#include <inttypes.h>
#include "emeter-toolkit.h"

int16_t rand16(void)
{
    static uint16_t rndnum;

    rndnum = rndnum*31821U + 13849U;
    return rndnum;
}
