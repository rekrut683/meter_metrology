/*******************************************************************************
 *  metrology-foreground.h -
 ******************************************************************************/

#if !defined(_METROLOGY_FOREGROUND_H_)
#define _METROLOGY_FOREGROUND_H_

extern struct metrology_data_s working_data;

/*! This variable is used as a series of flag bits for various purposes in the meter. */
extern uint16_t metrology_state;

/*! The current sampling rate, in samples per second. If the meter supports a very
    low power limp mode, this value may change with the mode of the meter. */
extern int16_t samples_per_second;

void set_phase_correction(struct phase_correction_s *s, int correction);

#if defined(__HAS_SD_ADC__)
void set_sd_phase_correction(struct phase_correction_sd_s *s, int phx, int correction);
#endif

/* Callback routines, which much be provided by the application */

/*! This function switches the meter to the normal operating state. It is usually used when the meter
    is in the low power state, or limp mode, and the conditions for normal operation have been detected.
    \brief Switch the meter to its normal operating mode. */
void switch_to_normal_mode(void);

/*! This function switches the meter to the limp, or live only, mode of operatiion. It is usually used
    the meter is running, but the measured voltage is too low to be valid.
    \brief Switch the meter to limp mode (also known as live only mode). */
void switch_to_limp_mode(void);

/*! This function switches the meter to the lowe power state, where only the RTC and power monitoring
    functions continue to operate within a timer interrupt routine. This is usually used when power
    failure has been detected. This function does not return until power has been restored. The
    machine sits in LPM3 within the function, waiting for actions within an interrupt routine to wake
    it up.
    \brief Switch the meter to minimum power operation (usual on supply failure). */
void switch_to_powerfail_mode(void);

#if defined(ESP_SUPPORT)
void esp_init(void);
void esp_start_measurement(void);
void esp_start_calibration(void);
void esp_set_active(void);
void esp_set_idle(void);
#endif

#endif
