/*******************************************************************************
 *  shift48.c - Shift a 48 bit number left or right.
 ******************************************************************************/

#include <inttypes.h>
#include "emeter-toolkit.h"

void shift48(int16_t x[3], int how_far)
{
#if defined(__MSP430__)
    int64_t tmp;
#endif

    if (how_far < 0)
    {
        while (how_far++)
        {
            tmp = x[2];
            tmp = tmp << 16;
            tmp |= x[1];
            tmp = tmp << 16;
            tmp |= x[0];
            tmp >>= 1;
            x[2] = (tmp >> 32) & 0xFFFF;
            x[1] = (tmp >> 16) & 0xFFFF;
            x[0] = tmp & 0xFFFF;
        }
    }
    else
    {
        while (how_far--)
        {
            tmp = x[2];
            tmp = tmp << 16;
            tmp |= x[1];
            tmp = tmp << 16;
            tmp |= x[0];
            tmp <<= 1;
            x[2] = (tmp >> 32) & 0xFFFF;
            x[1] = (tmp >> 16) & 0xFFFF;
            x[0] = tmp & 0xFFFF;
        }
    }
}
