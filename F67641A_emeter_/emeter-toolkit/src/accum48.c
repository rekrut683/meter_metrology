/*******************************************************************************
 *  accum48.c - Accumulate a 32 bit number into a 48 bit one.
 ******************************************************************************/

#include <inttypes.h>
#include <msp430.h>
#include "emeter-toolkit.h"

void accum48(int16_t x[3], int32_t y)
{
    int64_t acc;

    acc = (uint16_t) x[2];
    acc <<= 16;
    acc |= (uint16_t) x[1];
    acc <<= 16;
    acc |= (uint16_t) x[0];
    acc += y;
    x[0] = acc;
    acc >>= 16;
    x[1] = acc;
    acc >>= 16;
    x[2] = acc;
}

void accum48_48(int16_t z[3], int16_t y[3])
{
    int64_t acc_0;    // �������� �������
    int64_t acc_1;    // ���������

    acc_0 = (uint16_t) z[2];
    acc_0 <<= 16;
    acc_0 |= (uint16_t) z[1];
    acc_0 <<= 16;
    acc_0 |= (uint16_t) z[0];

    acc_1 = (uint16_t) y[2];
    acc_1 <<= 16;
    acc_1 |= (uint16_t) y[1];
    acc_1 <<= 16;
    acc_1 |= (uint16_t) y[0];

    acc_0 += acc_1;

    z[0] = acc_0;
    acc_0 >>= 16;
    z[1] = acc_0;
    acc_0 >>= 16;
    z[2] = acc_0;

}

void decum48_48(int16_t z[3], int16_t y[3])
{
    int64_t acc_0;    // �������� �������
    int64_t acc_1;    // ����������

    acc_0 = (uint16_t) z[2];
    acc_0 <<= 16;
    acc_0 |= (uint16_t) z[1];
    acc_0 <<= 16;
    acc_0 |= (uint16_t) z[0];

    acc_1 = (uint16_t) y[2];
    acc_1 <<= 16;
    acc_1 |= (uint16_t) y[1];
    acc_1 <<= 16;
    acc_1 |= (uint16_t) y[0];

    acc_0 -= acc_1;

    z[0] = acc_0;
    acc_0 >>= 16;
    z[1] = acc_0;
    acc_0 >>= 16;
    z[2] = acc_0;

}
