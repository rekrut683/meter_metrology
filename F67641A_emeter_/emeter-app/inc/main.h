/*******************************************************************************
 *  emeter-main.h -
 ******************************************************************************/

/*! \file */

#if !defined(_METER_MAIN_H_)
#define _METER_MAIN_H_

#if !defined(NO_ENERGY_ACCUMULATION)
enum
{
#if defined(ACTIVE_ENERGY_SUPPORT)  ||  defined(TOTAL_ACTIVE_ENERGY_SUPPORT)
    APP_ACTIVE_ENERGY_IMPORTED,
    APP_ACTIVE_ENERGY_EXPORTED,
#endif
#if defined(FUNDAMENTAL_ACTIVE_ENERGY_SUPPORT)  ||  defined(TOTAL_FUNDAMENTAL_ACTIVE_ENERGY_SUPPORT)
    APP_FUNDAMENTAL_ACTIVE_ENERGY_IMPORTED,
    APP_FUNDAMENTAL_ACTIVE_ENERGY_EXPORTED,
#endif
#if defined(REACTIVE_ENERGY_SUPPORT)  ||  defined(TOTAL_REACTIVE_ENERGY_SUPPORT)
    APP_REACTIVE_ENERGY_QUADRANT_I,
    APP_REACTIVE_ENERGY_QUADRANT_II,
    APP_REACTIVE_ENERGY_QUADRANT_III,
    APP_REACTIVE_ENERGY_QUADRANT_IV,
#endif
#if defined(FUNDAMENTAL_REACTIVE_ENERGY_SUPPORT)  ||  defined(TOTAL_FUNDAMENTAL_REACTIVE_ENERGY_SUPPORT)
    APP_FUNDAMENTAL_REACTIVE_ENERGY_QUADRANT_I,
    APP_FUNDAMENTAL_REACTIVE_ENERGY_QUADRANT_II,
    APP_FUNDAMENTAL_REACTIVE_ENERGY_QUADRANT_III,
    APP_FUNDAMENTAL_REACTIVE_ENERGY_QUADRANT_IV,
#endif
#if defined(APPARENT_ENERGY_SUPPORT)  ||  defined(TOTAL_APPARENT_ENERGY_SUPPORT)
    APP_APPARENT_ENERGY_IMPORTED,
    APP_APPARENT_ENERGY_EXPORTED,
#endif
#if defined(INTEGRATED_V2_SUPPORT)  ||  defined(TOTAL_INTEGRATED_V2_SUPPORT)
    APP_INTEGRATED_V2,
#endif
#if defined(INTEGRATED_I2_SUPPORT)  ||  defined(TOTAL_INTEGRATED_I2_SUPPORT)
    APP_INTEGRATED_I2,
#endif
    APP_TOTAL_ENERGY_BINS
};

/* These are all the possible types of consumed energies */
#if NUM_PHASES == 1
extern energy_t energy_consumed[1][APP_TOTAL_ENERGY_BINS];
#else
extern energy_t energy_consumed[NUM_PHASES + 1][APP_TOTAL_ENERGY_BINS];
#endif
#endif

#if defined(SAG_SWELL_SUPPORT)
extern uint16_t sag_events[NUM_PHASES];
extern uint32_t sag_duration[NUM_PHASES]; 
extern uint16_t swell_events[NUM_PHASES];
extern uint32_t swell_duration[NUM_PHASES]; 
#endif

#endif
