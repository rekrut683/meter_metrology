/*******************************************************************************
 *  emeter-app.h -
 ******************************************************************************/

/*! \file */

#if !defined(_METER_APP_H_)
#define _METER_APP_H_

#define SCFI0_LOW       (FLLD_1)        /* Freq = 1.024MHz */
#define SCFQCTL_LOW     (32 - 1)

#if defined(__MSP430_HAS_SD24_B__)
/* Run at 16.777216MHz */
#define DCO_CLOCK_SPEED 16
#define SCFI0_HIGH      (FN_3 | FLLD_4)
#define SCFQCTL_HIGH    (128 - 1)
#define IR_38K_DIVISOR  (220)
#elif defined(__MSP430_HAS_SD16_A3__)  ||  defined(__MSP430_HAS_SD16_A4__)  ||  defined(__MSP430_HAS_SD16_A6__)  ||  defined(__MSP430_HAS_SD16_A7__)
/* Run at 16.777216MHz */
#define DCO_CLOCK_SPEED 16
#define SCFI0_HIGH      (FN_3 | FLLD_4)
#define SCFQCTL_HIGH    (128 - 1)
#define IR_38K_DIVISOR  (220)
#else
/* Run at 8.388608MHz */
#define DCO_CLOCK_SPEED 8
#define SCFI0_HIGH      (FN_3 | FLLD_4)
#define SCFQCTL_HIGH    (64 - 1)
#define IR_38K_DIVISOR  (110)
#endif

/*! \brief Initialise all the data and peripherals, and prepare the machine to run
    after reset. */
void system_setup(void);

void display_startup_message(void);

#if defined(__MSP430__)  &&  defined(USE_WATCHDOG)
#define kick_watchdog()             WDTCTL = WDT_ARST_1000
#else
#define kick_watchdog()             /**/
#endif

#endif
