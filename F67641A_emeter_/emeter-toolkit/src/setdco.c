/*******************************************************************************
 *  setdco.c - Set the DCO frequency.
 ******************************************************************************/

#include <inttypes.h>
#include <msp430.h>
#include "emeter-toolkit.h"

#if defined(BCSCTL1_)  &&  defined(TACCR0_)
/* Set the DCO to a frequency specified in KHz */
void set_dco(int freq)
{
    unsigned int old_capture;
    unsigned int compare;
    unsigned int delta;
    
    delta = freq >> 4;

    CCTL2 = CCIS0 | CM0 | CAP;              /* Define CCR2, CAP, ACLK */
    TACTL = TASSEL1 | TACLR | MC1;          /* SMCLK, continuous mode */
    old_capture = 0;
    for (;;)
    {
        while (!(CCTL2 & CCIFG))
            /*dummy loop*/;                 /* Wait until capture occurs! */
        CCTL2 &= ~CCIFG;                    /* Capture occured, clear flag */
        compare = CCR2;                     /* Get current captured SMCLK */
        compare -= old_capture;             /* SMCLK difference */
        old_capture = CCR2;                 /* Save current captured SMCLK */
        if (delta == compare)
            break;                          /* If equal we are done */
        if (delta < compare)
        {
            /* DCO is too fast, slow it down */
            DCOCTL--;
            /* Did DCO role under? */
            if (DCOCTL == 0xFF)
                BCSCTL1--;                  /* Select next lower RSEL */
        }
        else
        {
            DCOCTL++;
            /* Did DCO role over? */
            if (DCOCTL == 0x00)
                BCSCTL1++;                  /* Select next higher RSEL */
        }
    }
    /* Stop CCR2 function */
    CCTL2 = 0;
    /* Stop Timer_A */
    TACTL = 0;
}
#endif
