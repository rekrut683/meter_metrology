/*******************************************************************************
 *  isqrt32.c - Square root of a 32 bit number.
 ******************************************************************************/

#include <inttypes.h>
#include "emeter-toolkit.h"

uint32_t isqrt32(uint32_t h)
{
    uint32_t x;
    uint32_t y;
    int i;

#if defined(__GNUC__)  &&  defined(__MSP430__)
    x = 0x80000000;
    y = 0;
    i = 32;
    __asm__ (
        "1: \n"
        " setc \n"
        " rlc   %A[x] \n"
        " rlc   %B[x] \n"
        " sub   %A[x],%A[y] \n"
        " subc  %B[x],%B[y] \n"
        " jhs   2f \n"
        " add   %A[x],%A[y] \n"
        " addc  %B[x],%B[y] \n"
        " sub   #2,%A[x] \n"
        "2: \n"
        " inc   %A[x] \n"
        " rla   %A[h] \n"
        " rlc   %B[h] \n"
        " rlc   %A[y] \n"
        " rlc   %B[y] \n"
        " rla   %A[h] \n"
        " rlc   %B[h] \n"
        " rlc   %A[y] \n"
        " rlc   %B[y] \n"
        " dec   %[i] \n"
        " jne   1b \n"
        : [x] "+r"(x), [y] "+r"(y)
        : [h] "r"(h), [i] "r"(i));
#else
    x =
    y = 0;
    for (i = 0;  i < 32;  i++)
    {
        x = (x << 1) | 1;
        if (y < x)
            x -= 2;
        else
            y -= x;
        x++;
        y <<= 1;
        if ((h & 0x80000000))
            y |= 1;
        h <<= 1;
        y <<= 1;
        if ((h & 0x80000000))
            y |= 1;
        h <<= 1;
    }
#endif
    return  x;
}
