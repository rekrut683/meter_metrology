/*******************************************************************************
 *  metrology-corrections.h -
 ******************************************************************************/

#if defined(DYNAMIC_FREQUENCY_RELATED_CORRECTION_SUPPORT)
    #if NUM_PHASES == 1
        #if defined(NEUTRAL_MONITOR_SUPPORT)
void dynamic_frequency_related_correction(int ch);
        #else
void dynamic_frequency_related_correction(void);
        #endif
    #else
void dynamic_frequency_related_correction(struct phase_parms_s *phase, struct phase_calibration_data_s const *phase_cal, int ph);
    #endif
#endif

#if defined(DYNAMIC_CURRENT_RELATED_CORRECTION_SUPPORT)
    #if NUM_PHASES == 1
        #if defined(NEUTRAL_MONITOR_SUPPORT)
void dynamic_current_related_correction(int ch);
        #else
void dynamic_current_related_correction(void);
        #endif
    #else
void dynamic_current_related_correction(struct phase_parms_s *phase, struct phase_calibration_data_s const *phase_cal, int ph);
    #endif
#endif
