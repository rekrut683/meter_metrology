/*******************************************************************************
 *  q1_15_mul.c - Multiply two 16 bit numbers
 ******************************************************************************/

#include <inttypes.h>
#include <msp430.h>
#include "emeter-toolkit.h"

uint16_t rndnum;
uint16_t tpdnum;

int16_t Q1_15_mulq(register int16_t x, register int16_t y)
{
    int16_t x1;
    int32_t z;

    z = ((int32_t) x*y) + tpdnum;
    x1 = (z >> 15);

    return x1;
}

void tpd_gen(void)
{
    int16_t x1;
    int16_t x2;

    x1 = rndnum;
    x2 = x1*31821U;

    x2 = (x2 + 13849) & 0x3FFF;
    tpdnum = rndnum + x2;
    rndnum = x2;
}

int16_t Q1_15_mulr(int16_t x, int16_t y)
{
    int16_t x1;
    int32_t z;

    z = (int32_t) x*y;
    z += 0x4000;
    x1 = (z >> 15);
    return x1;
}

int16_t q1_15_mul(int16_t x, int16_t y)
{
    int16_t x1;
    int32_t z;

    z = (int32_t) x*y;
    x1 = (z >> 15);
    return x1;
}
