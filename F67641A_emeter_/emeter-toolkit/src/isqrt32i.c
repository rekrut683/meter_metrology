/*******************************************************************************
 *  isqrt32i.c - Square root of a 32 bit number.
 ******************************************************************************/

#include <inttypes.h>
#include "emeter-toolkit.h"

uint16_t isqrt32i(uint32_t h)
{
    uint32_t res32;
    uint16_t res16;

    res32 = isqrt32(h);
    res16 = res32 >> 16;
    if (res32 & 0x8000LLU)
    {
        if (res16 != 0xFFFFLU)
            return res16 + 1;
    }
    return res16;
}
