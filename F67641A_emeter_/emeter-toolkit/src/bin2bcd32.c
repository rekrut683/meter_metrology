/*******************************************************************************
 *  bin2bcd32.c - 32 bit binary to BCD conversion.
 ******************************************************************************/

#include <inttypes.h>
#include <msp430.h>
#include <stdio.h>
#include "emeter-toolkit.h"

void bin2bcd32(uint8_t bcd[5], uint32_t bin)
{
    int i;
    char buf[10 + 1];
    
    //sprintf (buf, "%010ld", bin);
    for (i = 0;  i < 5;  i++) {
        bcd[i] = ((buf[2*i] & 0x0F) << 4) | (buf[2*i + 1] & 0x0F);
    }
}   
