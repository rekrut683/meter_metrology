/*******************************************************************************
 *  dc_filter16.c - DC estimation and removal for 16 bit signals
 ******************************************************************************/

#include <inttypes.h>
#include "msp430.h"
#include "emeter-toolkit.h"

int16_t dc_filter16(int32_t *p, int16_t x)
{
    *p += ((((int32_t) x << 16) - *p) >> 14);
    x -= (*p >> 16);
    return x;
}

int16_t dc_filter16_no_update(const int32_t *p, int16_t x)
{
    return x - (*p >> 16);
}

void dc_filter16_init(int32_t *p, int16_t x)
{
    *p = (int32_t) x << 16;
}

int32_t dc_filter16_estimate(const int32_t *p)
{
    return *p >> 8;
}

