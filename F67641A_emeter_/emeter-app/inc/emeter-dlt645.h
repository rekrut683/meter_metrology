/*******************************************************************************
 *  emeter-dlt645.h -
 ******************************************************************************/

/*! \file */

#if !defined(_EMETER_DLT645_H_)
#define _EMETER_DLT645_H_

void dlt645_rx_restart(void);

void dlt645_rx_byte(int port, uint8_t c);

void dlt645_service(void);

#endif
