/*******************************************************************************
 *  mul48.c - Multiply a 32 bit number by a 16 bit number, and return the top
 *            32 bits of the result.
 ******************************************************************************/

#include <inttypes.h>
#include "emeter-toolkit.h"

int32_t mul48(int32_t x, int16_t y)
{
    int32_t z;
    int16_t residue;
#if defined(EMETER_TOOLKIT_SUPPORT_64BIT)
    int64_t tmp;
#endif

    z = x & 0xFFFF;
    z *= y;
    residue = z & 0xFFFF;
    z >>= 16;
    x >>= 16;
    x *= y;
    z += x;

    tmp = z;
    tmp <<= 1;
    if ((residue & 0x8000))
        tmp |= 1;
    z = tmp;

    return z;
}

int32_t mul48_32_16(int32_t x, uint16_t y)
{
    int64_t z;

    z = (int64_t) x*(int64_t) y;
    return z >> 16;
}

uint32_t mul48u_32_16(uint32_t x, uint16_t y)
{
    uint64_t z;

    z = (int64_t) x*(int64_t) y;
    return z >> 16;
}
