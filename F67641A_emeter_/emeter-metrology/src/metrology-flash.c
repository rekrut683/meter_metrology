/*******************************************************************************
 *  metrology-flash.c - Flash reading and writing routines.
 ******************************************************************************/

#include <signal.h>
#include <inttypes.h>
#if defined(__MSP430__)
#include <msp430.h>
#endif
#include <emeter-toolkit.h>

#include "metrology-flash.h"

#if defined(__MSP430__)

void flash_clr(int16_t *ptr)
{
    _DINT();
#if defined(__MSP430I4020__)
    if ((FCTL3 & LOCKSEG))
        FCTL3 = FWKEY | LOCKSEG;
#else
    FCTL3 = FWKEY;                      /* Lock = 0 */
#endif
    FCTL1 = FWKEY | ERASE;
    *((int *) ptr) = 0;                 /* Erase flash segment */
    _EINT();
}

void flash_write_int8(int8_t *ptr, int8_t value)
{
    _DINT();
#if defined(__MSP430I4020__)
    if ((FCTL3 & LOCKSEG))
        FCTL3 = FWKEY | LOCKSEG;
#else
    FCTL3 = FWKEY;                      /* Lock = 0 */
#endif
    FCTL1 = LOCK | WRT;
    *((int8_t *) ptr) = value;          /* Program the flash */
}

void flash_write_int16(int16_t *ptr, int16_t value)
{
    _DINT();
#if defined(__MSP430I4020__)
    if ((FCTL3 & LOCKSEG))
        FCTL3 = FWKEY | LOCKSEG;
#else
    FCTL3 = FWKEY;                      /* Lock = 0 */
#endif
    FCTL1 = FWKEY | WRT;
    *((int16_t *) ptr) = value;         /* Program the flash */
}

void flash_write_int32(int32_t *ptr, int32_t value)
{
    _DINT();
#if defined(__MSP430I4020__)
    if ((FCTL3 & LOCKSEG))
        FCTL3 = FWKEY | LOCKSEG;
#else
    FCTL3 = FWKEY;                      /* Lock = 0 */
#endif
    FCTL1 = FWKEY | WRT;
    *((int32_t *) ptr) = value;         /* Program the flash */
}

void flash_memcpy(void *to, const void *from, int len)
{
    const uint8_t *fromx;
    uint8_t *tox;

    fromx = (const uint8_t *) from;
    tox = (uint8_t *) to;
    _DINT();
#if defined(__MSP430I4020__)
    if ((FCTL3 & LOCKSEG))
        FCTL3 = FWKEY | LOCKSEG;
#else
    FCTL3 = FWKEY;                      /* Lock = 0 */
#endif
    FCTL1 = FWKEY | WRT;
    while (len)
    {
        *tox++ = *fromx++;
        len--;
    }
}

void flash_secure(void)
{
    _DINT();
    FCTL1 = FWKEY;                      /* Erase, write = 0 */
#if defined(__MSP430I4020__)
    if (!(FCTL3 & LOCKSEG))
        FCTL3 = FWKEY | LOCKSEG;
#else
    FCTL3 = FWKEY | LOCK;               /* Lock = 1 */
#endif
    _EINT();
}
#endif
